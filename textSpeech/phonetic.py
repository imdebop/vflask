from types import ClassMethodDescriptorType
from typing import Dict, List
import random

class PhoneticData():
    @staticmethod
    def get():
        data = {
            1: "alfa", 2: "bravo", 3: "Charlie", 4: "delta", 5: "echo", 6: "foxtrot",
            7: "golf", 8: "hotel", 9: "India", 10: "Juliett", 11: "kilo", 12: "Lima",
            13: "Mike", 14: "November", 15: "Oscar", 16: "Papa", 17: "Quebec", 18: "Romeo",
            19: "Sierra", 20: "tango", 21: "uniform", 22: "victor", 23: "whisky", 24: "x-ray",
            25: "Yankee", 26: "Zulu"
        }
        return data

class Phonetic():
    wordDict:  Dict[int, str] = PhoneticData.get()
    print("word data loaded.")

    @classmethod
    def createWordList(cls, numOfWords: int) -> List[str]:
        seed = [random.randint(1, 26) for i in range(numOfWords)]
        return [ cls.wordDict[i] for i in seed]
    