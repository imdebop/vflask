def todoitem_create(db):
    class ToDoItem(db.Model):
        __tablename__='todoitems'
        item_id = db.Column(db.Integer, primary_key=True)
        title = db.Column(db.String(100), nullable=False)
        done = db.Column(db.Boolean, nullable=False, default=False)
    return ToDoItem
