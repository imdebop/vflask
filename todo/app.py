#https://www.atmarkit.co.jp/ait/articles/1807/31/news042.html

#reference for Mongo
#https://stackabuse.com/integrating-mongodb-with-flask-using-flask-pymongo/

from flask import Flask, render_template, redirect, request
from flask_sqlalchemy import SQLAlchemy
from todo import ToDoList

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../instance/todo.sqlite'
db = SQLAlchemy(app)

from models import todo
todoItem = todo.todoitem_create(db)

todolist = ToDoList()


@app.route('/')
def show_todolist():
    return render_template("showtodo.html", todolist = todolist.get_all())

@app.route('/additem', methods=['POST'])
def add_item():
    title = request.form['title']
    if not title:
        return redirect('/')
    
    todolist.add(title)
    db.session.add(todoItem(title=title))
    db.session.commit()
    return redirect('/')

@app.route('/deleteitem/<int:item_id>')
def delete_todoitem(item_id):
    todolist.delete(item_id)
    #print(len(todolist))
    return redirect('/')

@app.route('/updatedone/<int:item_id>')
def update_todoitemdone(item_id):
    todolist.update(item_id)
    return redirect('/')

@app.route('/deletealltodoitems')
def delete_alltodoitems():
    todolist.delete_doneitem()
    return redirect('/')

